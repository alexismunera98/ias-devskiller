package com.devskiller.tasks.blog.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
public class Comment {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String comment;

    public Comment(String comment, String author) {
        this.comment = comment;
        this.author = author;
//        this.creationDate = creationDate;
    }

    private String author;

    private LocalDateTime creationDate;

    public Long getId() {
        return id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }
}
