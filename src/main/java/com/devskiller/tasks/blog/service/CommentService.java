package com.devskiller.tasks.blog.service;

import java.util.ArrayList;
import java.util.List;

import com.devskiller.tasks.blog.model.Comment;
import com.devskiller.tasks.blog.repository.CommentRepository;
import com.devskiller.tasks.blog.repository.PostRepository;
import org.springframework.stereotype.Service;

import com.devskiller.tasks.blog.model.dto.CommentDto;
import com.devskiller.tasks.blog.model.dto.NewCommentDto;

@Service
public class CommentService {

	private final PostRepository postRepository;
	private final CommentRepository commentRepository;

	public CommentService(PostRepository postRepository, CommentRepository commentRepository) {
		this.postRepository = postRepository;
		this.commentRepository =  commentRepository;
	}

	/**
	 * Returns a list of all comments for a blog post with passed id.
	 *
	 * @param postId id of the post
	 * @return list of comments sorted by creation date descending - most recent first
	 */
	public List<CommentDto> getCommentsForPost(Long postId) {
//		throw new UnsupportedOperationException();
		List<CommentDto> lista = new ArrayList<>();
		postRepository.findById(postId).get().getComments().forEach(comment -> {
			lista.add(new CommentDto(comment.getId(), comment.getComment(), comment.getAuthor(),comment.getCreationDate()));
		});
		return lista;
	}

	/**
	 * Creates a new comment
	 *
	 * @param newCommentDto data of new comment
	 * @return id of the created comment
	 *
	 * @throws IllegalArgumentException if there is no blog post for passed newCommentDto.postId
	 */
	public Long addComment(NewCommentDto newCommentDto) {
		Comment newComment = new Comment(newCommentDto.getContent(), newCommentDto.getAuthor());
		Comment savedComment = commentRepository.save(newComment);
		return savedComment.getId();
//		throw new UnsupportedOperationException();
	}
}

